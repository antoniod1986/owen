import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { EmployeeComponent } from './components/employee/employee.component';
import { GridComponent } from './components/grid/grid.component';
import { TruncateModule } from 'ng2-truncate';
import { EmployeeService } from './services/employee.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { FilterComponent } from './components/filter/filter.component';
import { TagInputModule } from 'ng2-tag-input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


@NgModule({
  declarations: [
    AppComponent,
    EmployeeComponent,
    GridComponent,
    FilterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    TruncateModule,
    HttpClientModule,
    TagInputModule,
    BrowserAnimationsModule
  ],
  providers: [
    EmployeeService,
    HttpClient
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
