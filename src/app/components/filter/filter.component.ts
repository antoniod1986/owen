import { Component, OnInit } from '@angular/core';
import {EmployeeService} from '../../services/employee.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {
  items = [];
  constructor(
    private employeeService: EmployeeService
  ) { }

  ngOnInit() {
  }
  onItemAdded() {
    this.employeeService.tags = this.items;
  }
  onItemRemoved() {
    this.employeeService.tags = this.items;
  }
}
