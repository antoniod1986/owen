import { Component, Input, OnInit } from '@angular/core';
import { Employee } from '../../interfaces/employee';
import { EmployeeService } from '../../services/employee.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {
  @Input() employee: Employee;
  @Input() employeeIndex: number;
  @Input() activeTags: any;
  constructor(
    private employeeService: EmployeeService
  ) { }

  ngOnInit() {
    this.activeTags = this.employeeService.tags || [];
    this.employeeService.getTags
      .subscribe((tags) => {
        this.activeTags = tags || [];
      });
  }

  filter(employee) {
    for (const tag of this.activeTags) {
      if (employee.skills.indexOf(tag.value.toLowerCase()) !== -1) {
        continue;
      } else {
        return false;
      }
    }
    return true;
  }

}
