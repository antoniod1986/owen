import {EventEmitter, Injectable, OnInit} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Employees } from '../interfaces/employees';
import {debug} from 'util';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';

@Injectable()
export class EmployeeService implements OnInit {
  private _employees: Employees;
  private _tags: Array<string>;
  getTags: EventEmitter<Array<string>> = new EventEmitter();
  constructor(
    private http: HttpClient
  ) { }

  ngOnInit(): void { }

  getEmployees(): Observable<Employees> {
    return this.http.get<Employees>('https://sys4.open-web.nl/employees.json')
      .map(this.extractData);
  }
  get tags(): Array<string> {
    return this._tags;
  }
  set tags(__tags: Array<string>) {
    this._tags = __tags;
    this.getTags.emit(this._tags);
  }
  get employees(): Employees {
    return this._employees;
  }
  set employees(__employees: Employees) {
    this._employees = __employees;
  }
  private extractData(response) {
    this.employees = response.employees || [];
    return this.employees;
  }
}
