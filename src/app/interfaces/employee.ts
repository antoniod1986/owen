export interface Employee {
  name: string;
  role: string;
  bio?: string;
  skills?: Array<string>;
  profileImage: string;
}
