import { OwenPage } from './app.po';

describe('owen App', () => {
  let page: OwenPage;

  beforeEach(() => {
    page = new OwenPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
